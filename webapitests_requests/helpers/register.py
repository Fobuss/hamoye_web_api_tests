import requests
import settings
import common


class RegisterHelper(object):
    PATH = '/core/users'

    def init_body(self, auth_type=None, email=None, pwd=None, name=None, remember=None):
        body = {
            "authType": auth_type or "DbAuthTypeInternal",
            "authAccountId": email or common.rand_gmail(),
            "password": pwd or common.rand_str(),
            "name": name or common.rand_str(),
            "remember": remember if remember is not None else False
        }
        return body

    # may be parametrized for DDT
    def register(self, body):

        response = requests.post(
            settings.URL + self.PATH,
            json=body
        )

        return response.status_code, response.json()

    def check_ok(self, response):
        return sorted(response.keys()) == sorted(['sessionId', 'userId', 'accountId'])
