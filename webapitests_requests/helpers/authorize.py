import requests
import settings


class AuthorizeHelper(object):

    PATH = '/acl/auth/{0}/{1}'

    # may be parametrized for DDT
    def authorize(self, auth_type, email, pwd):

        body = {
            "password": pwd,
        }

        response = requests.post(
            settings.URL + self.PATH.format(auth_type, email),
            json=body
        )

        return response.status_code, response.json()
