from base_test import BaseTestCase
import settings
from helpers.authorize import AuthorizeHelper
from helpers.register import RegisterHelper


class AuthorizeTestCase(BaseTestCase):

    def test_authorize(self):
        register_helper = RegisterHelper()
        authorize_helper = AuthorizeHelper()

        body = register_helper.init_body()
        status, data = register_helper.register(body)

        self.assertEqual(status, settings.HTTP_OK)
        register_helper.check_ok(data)

        response = authorize_helper.authorize(body['authType'], body['authAccountId'], body['password'])
        self.assertEqual(response.status_code, settings.HTTP_OK)


