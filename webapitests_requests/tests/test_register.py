from base_test import BaseTestCase
import settings
from helpers.register import RegisterHelper


class RegisterTestCase(BaseTestCase):

    def test_register(self):
        register_helper = RegisterHelper()

        body = register_helper.init_body()
        status, data = RegisterHelper().register(body)

        self.assertEqual(status, settings.HTTP_OK)
        self.assertTrue(register_helper.check_ok(data))
