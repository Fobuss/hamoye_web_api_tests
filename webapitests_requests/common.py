import random
import string
import time
import settings

DEFAULT_SIZE = 8


def rand_str(size=DEFAULT_SIZE):
    return ''.join(random.choice(string.ascii_letters) for _ in range(size))


def rand_gmail():
    return settings.GMAIL % int(time.time())
